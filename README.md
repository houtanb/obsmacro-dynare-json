# Using the Dynare Preprocessor’s JSON Output: An Example

This repository contains the code for the above-titled blog post on the [CEPREMAP Macro Observatory blog](https://macro.nomics.world/).

To run the code described in the model, do the following:
 - `git clone --depth 1 https://github.com/houtanb/obsmacro-dynare-json.git`
 - Download [Dynare 4.6 or later](http://www.dynare.org/download/) for your system.
 - Open Matlab or Octave and type:
   - `addpath <<path to dynare>>/matlab`
   - `addpath <<path to obsmacro-dynare-json/ols>>`
   - `cd <<path to obsmacro-dynare-json/afv2013table1>>`
   - `dynare afv2013table1.mod`
   - `cd <<path to obsmacro-dynare-json/sw2007>>`
   - `dynare Smets_Wouters_2007.mod`
